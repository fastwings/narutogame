if( typeof createjs == 'undefined' || !createjs.PreloadJS ) {
	define([
			'Console', 
			'order!libs/vendors/preloadjs/preloadjs.min' 
		], function (Console) {
		Console.group("Entering Preload js module.");
		Console.info("Preload js: ", createjs.PreloadJS);
		Console.groupEnd();
		return createjs;
	});
}