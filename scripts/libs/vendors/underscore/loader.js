if( typeof _ == 'undefined'  ) {
	define([
			'Console', 
			'order!libs/vendors/underscore/underscore' 
		], function (Console) {
		Console.group("Entering Underscre module & Underscore String module.");
		Console.info("Underscre: ", _);
		Console.groupEnd();
		return _;
	});
}