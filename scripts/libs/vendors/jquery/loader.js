if( typeof window.jQuery == 'undefined' ) {
	define(['Console', 'order!libs/vendors/jquery/jquery'], function (Console) {
		Console.group("Entering jQuery module.");
		Console.info("jQuery: ", $);
		Console.groupEnd();
		return $;
	});
}