if( typeof createjs == 'undefined' || !createjs.SoundJS ) {
	define([
			'Console', 
			'order!libs/vendors/soundjs/soundjs.min' 
		], function (Console) {
		Console.group("Entering Sound js module.");
		Console.info("Sound js: ", createjs.SoundJS);
		Console.groupEnd();
		return createjs;
	});
}