if( typeof moment == 'undefined') {
	define([
			'Console', 
			'order!libs/vendors/moment/moment' 
		], function (Console) {
		Console.group("Entering Moment module.");
		Console.info("Moment: ", moment);
		Console.groupEnd();
		return moment;
	});
}