if( typeof createjs == 'undefined' || !createjs.Tween  ) {
	define([
			'Console', 
			'order!libs/vendors/tweenjs/tweenjs.min' 
		], function (Console) {
		Console.group("Entering Tween js module.");
		Console.info("Tween js: ", createjs);
		Console.groupEnd();
		return createjs;
	});
}