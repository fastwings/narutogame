if( typeof createjs == 'undefined' || !createjs.EaselJS ) {
	define([
			'Console', 
			'order!libs/vendors/easejs/utils/UID' ,
			'order!libs/vendors/easejs/ui/ButtonHelper' ,
			'order!libs/vendors/easejs/ui/Touch' ,
			'order!libs/vendors/easejs/geom/Matrix2D' ,
			'order!libs/vendors/easejs/geom/Point' ,
			'order!libs/vendors/easejs/geom/Rectangle' ,
			'order!libs/vendors/easejs/events/EventDispatcher' ,
			'order!libs/vendors/easejs/events/MouseEvent' ,
			'order!libs/vendors/easejs/filters/Filter' ,
			'order!libs/vendors/easejs/filters/ColorFilter' ,
			'order!libs/vendors/easejs/filters/AlphaMapFilter' ,
			'order!libs/vendors/easejs/filters/AlphaMaskFilter' ,
			'order!libs/vendors/easejs/filters/BoxBlurFilter' ,
			'order!libs/vendors/easejs/filters/ColorMatrix' ,
			'order!libs/vendors/easejs/filters/ColorMatrixFilter' ,
			'order!libs/vendors/easejs/display/DisplayObject' ,
			'order!libs/vendors/easejs/display/DomElement' ,
			'order!libs/vendors/easejs/display/Container' ,
			'order!libs/vendors/easejs/display/Graphics' ,
			'order!libs/vendors/easejs/display/Bitmap' ,
			'order!libs/vendors/easejs/display/Shape' ,
			'order!libs/vendors/easejs/display/Stage' ,
			'order!libs/vendors/easejs/display/BitmapAnimation' ,
			'order!libs/vendors/easejs/display/Shadow' ,
			'order!libs/vendors/easejs/display/Text' ,
			'order!libs/vendors/easejs/utils/Ticker' ,
			'order!scripts/libs/vendors/easejs/display/SpriteSheet.js' ,
			'order!libs/vendors/easejs/utils/SpriteSheetUtils' ,
			'order!libs/vendors/easejs/utils/SpriteSheetBuilder' ,
		], function (Console) {
		Console.group("Entering Easeljs module.");
		Console.info("Easeljs: ", createjs);
		Console.groupEnd();
		return createjs;
	});
}