define([
	// Standard Libs
	'Console'		// lib/console/console
	, 'jQuery'	// lib/underscore/underscore
	, 'Underscore'	// lib/underscore/underscore
	, 'Moment'	// lib/moment/moment
	// Basic Vendors Libs For the Game
	, "PreloadJs"
	, "EaseJs"
	, "TweenJs"
	, "SoundJs"
], function (Console,$, _,moment) {
	"use strict";
	var object = function (elementId) {
		var _this = this;

			Console.group("Game Logger.");
			Console.info("Game Init");
			// access stage object
			var canvas = document.getElementById(elementId);

			Console.info(canvas,elementId);
			var stage = new createjs.Stage(canvas);
			var canvasWidth = canvas.width;
			var canvasHeight = canvas.height;
			//make global params
			_this.canvasCtx = canvas;
			_this.canvasWidth = canvasWidth;
			_this.canvasHeight = canvasHeight;

			// enable touch interactions if supported on the current device:
			createjs.Touch.enable(stage);
			// enabled mouse over / out events
			stage.enableMouseOver(10);
			stage.mouseMoveOutside = true; // keep tracking the mouse even when it leaves the canvas
			// create stage size
			_this.stage = stage;
		_this.Init = function () {

			createjs.Ticker.setFPS(30);
			createjs.Ticker.addListener(NarutoGame);
			_this.engageTick();
		}
		_this.Run = function() {
	    var menu_screen = new NarutoGame_Logic.Menu.UI({
	    	app: _this
	    });
	    _this.stage.addChild(menu_screen);
	    _this.stage.update();


			Console.groupEnd();
		}
		_this.ChangeMap = function (mapType,row,cols) {

			Console.group("Game Logger.");
			_this.clearStage();
	    _this.Init();
			Console.info("init Map Change");
			var map = new NarutoGame_Logic.Maps.Manager({
	    			app: _this,
						mapType:mapType,
						row: row,
						cols: cols
			});
			Console.info("Game Map Change",map);
			//TODO need add player
	    _this.stage.addChild(map);
	    _this.stage.update();

			Console.groupEnd();
		}
		_this.engageTick = function() {
			NarutoGame.tick = function(){
				stage.update();
			}
		}
		_this.clearStage = function() {
			_this.stage.removeAllChildren();
			_this.stage.update();
			_this.stage.clear();
		}
	}
	object.prototype.toString = function() {
		return "[Naruto Sivan Game]";
	};
	window.NarutoGame = object;
});