if( !window.NarutoGame  ) {
	define([
			'Console',
			'order!libs/game/common' ,
			'order!libs/game/assetPipe' , 
			'order!libs/game/common/TextLink' , 
			'order!libs/game/menu/ui.animation' ,
			'order!libs/game/menu/ui.button' ,
			'order!libs/game/menu/ui' ,
			'order!libs/game/maps/levels/test.level' ,
			'order!libs/game/maps/manager' ,
			'order!libs/game/main' 
		], function (Console) {
		Console.group("Entering Game module.");
		Console.info("Game MainObject : ", window.NarutoGame);
		Console.groupEnd();
		return window.NarutoGame;
	});
}