define([
	// Standard Libs
	'Console'		// lib/console/console
	, 'jQuery'	// lib/underscore/underscore
	, 'Underscore'	// lib/underscore/underscore
	, 'Moment'	// lib/moment/moment
	// Basic Vendors Libs For the Game
	, "PreloadJs"
	, "EaseJs"
	, "TweenJs"
	, "SoundJs"
], function (Console,$, _,moment) {
	"use strict";
	var object = function(props) { this.initialize(props); };
	var obj = object.prototype = new createjs.Container();
	obj.Container_initialize = obj.initialize;
	obj.events = {};
	obj.events.onInitAssetPipe = function (){
		obj.ApplicationObject.stage.autoClear = false;
	}
	obj.events.onWaitingAssetPipe = function (){}

	obj.events.onCompleteAssetPipe = function (assets){
		obj.assets = assets;
		console.log("[Menu Assets Loaded]",assets);
		obj.initAnimation();
	}
	obj.initialize = function (props) {
		var _this = this;
		if (props) {
			obj.Container_initialize();
			obj.ApplicationObject = props.app;
			obj.X = props.x;
			obj.Y = props.y;
			obj.props = props;
			obj.assetPipe = new NarutoGame_Logic.AssetPipe(menu_animation_menifiest,obj.events.onInitAssetPipe , obj.events.onWaitingAssetPipe,obj.events.onCompleteAssetPipe);
			obj.assetPipe.Load();
		}
	}
	obj.initAnimation = function() {
		Console.group("Menu Animation");
		obj.continer =new createjs.Container(); 
		obj.slider_bullton_Continer = new createjs.Container();
		obj._buildSlider();
		obj._startTimerSlider();
		obj.addChild(obj.continer);
		Console.groupEnd();
	}
	obj._startTimerSlider = function () {
		var firstElmName ="slider_btn_1";
		obj.changeSliderImage(1);
		var mainElem = _.find(obj.slider_bullton_Continer.children , function (item) {return item.name == firstElmName});
		var childElem = _.find(obj.slider_bullton_Continer.children , function (item) {return item.idx == "s"+mainElem.id});
		var pos_x = childElem.last_x;
		var pos_y = childElem.last_y;
		childElem.graphics.clear().setStrokeStyle(1).beginStroke(createjs.Graphics.getRGB(255,255,255)).beginFill(createjs.Graphics.getRGB(0,0,0)).drawCircle(pos_x  , pos_y ,sessionStorage.current_menu_silder_radios);
		sessionStorage.is_blocked = 0;
		sessionStorage.current_menu_silder_idx = 1;
		obj.timer_current_time = moment();
		sessionStorage.interval  = 4;
		obj.timer = setTimeout(obj.onTick,100);
	}
	obj.changeSliderImage = function (idx) {
		var slider_name = "slider_item_"+idx;
		var relativePos_x = obj.X + 13;
		var relativePos_y = obj.Y + 5;
		var slider_continer_name = "slider_img";
		var slider_asset = _.find(obj.assets,function (item) {return item.id == slider_name});
		var slider_continer = _.find(obj.continer.children,function (item) {return item.name == slider_continer_name});

		console.log(slider_asset,slider_continer,idx);
		if (slider_continer == undefined) {
			var bk_grap = new createjs.Graphics();
			bk_grap.beginBitmapFill(slider_asset.result,"no-repeat");
			var background = new createjs.Shape(bk_grap.drawRect(0,0,slider_asset.width,slider_asset.height));
			background.alpha = 0.7;
			background.x = relativePos_x;
			background.y = relativePos_y;
			background.name = "slider_img";

			background.onMouseOver =function(evt) {
				var target = evt.target;
				background.alpha = 1;
				sessionStorage.is_blocked = 1;

			}
			background.onMouseOut = function(evt) {
				var target = evt.target;
				background.alpha = 0.7;
				sessionStorage.is_blocked = 0;
			}
			console.log(background);
			obj.continer.addChild(background);
		}
		else {
			slider_continer.graphics.clear();
			slider_continer.graphics.beginBitmapFill(slider_asset.result).drawRect(0,0,slider_asset.width,slider_asset.height);
			slider_continer.alpha = 0.7;
			slider_continer.x = relativePos_x;
			slider_continer.y = relativePos_y;

		}
	}
	obj.onTick = function () {
		if (parseInt(sessionStorage.is_blocked) == 0) {
			if (moment().diff(obj.timer_current_time,'seconds')>= sessionStorage.interval) {
				//reset all markers
				obj._reset_slider_btns();
				//first the slider
				var idx = parseInt(sessionStorage.current_menu_silder_idx);
				var maxItem = parseInt(sessionStorage.current_menu_silder_max);
				var elmName ="slider_btn_"+idx;
				var mainElem = _.find(obj.slider_bullton_Continer.children , function (item) {return item.name == elmName});
				var childElem = _.find(obj.slider_bullton_Continer.children , function (item) {return item.idx == "s"+mainElem.id});
				var pos_x = childElem.last_x;
				var pos_y = childElem.last_y;
				childElem.graphics.clear().setStrokeStyle(1).beginStroke(createjs.Graphics.getRGB(255,255,255)).beginFill(createjs.Graphics.getRGB(0,0,0)).drawCircle(pos_x  , pos_y ,sessionStorage.current_menu_silder_radios);
				
				obj.changeSliderImage(idx);
				if (idx >= maxItem)
					sessionStorage.current_menu_silder_idx = 1;
				else
					sessionStorage.current_menu_silder_idx = idx +1;
				obj.timer_current_time = moment();
			}
		}
	}
	obj._reset_slider_btns = function() {
			var mainElems = _.where(obj.slider_bullton_Continer.children , {is_slider_btn:true});
			Console.info(mainElems);
			_.each(mainElems,function (item){
				var elemId = item.id
				var childElem = _.find(obj.slider_bullton_Continer.children , function (item) {return item.idx == "s"+elemId});
				var pos_x = childElem.last_x;
				var pos_y = childElem.last_y;
				childElem.graphics.clear().setStrokeStyle(1).beginStroke(createjs.Graphics.getRGB(255,255,255)).beginFill(createjs.Graphics.getRGB(255,255,255)).drawCircle(pos_x  , pos_y ,sessionStorage.current_menu_silder_radios);
			});
	}
	obj.backLastKnownPosSlider = function () {
		var idx = parseInt(sessionStorage.current_menu_silder_idx);
		var elmName ="slider_btn_"+idx;
		var mainElem = _.find(obj.slider_bullton_Continer.children , function (item) {return item.name == elmName});
		var childElem = _.find(obj.slider_bullton_Continer.children , function (item) {return item.idx == "s"+mainElem.id});
		var pos_x = childElem.last_x;
		var pos_y = childElem.last_y;
		childElem.graphics.clear().setStrokeStyle(1).beginStroke(createjs.Graphics.getRGB(255,255,255)).beginFill(createjs.Graphics.getRGB(0,0,0)).drawCircle(pos_x  , pos_y ,sessionStorage.current_menu_silder_radios);
	
	}
	obj._buildSlider = function(){
		var maxBulltin = 11;
		var bulltion_radios = 5;
		var bulltion_small_radios = 3;
		var relativePos_x = obj.X + 25;
		var relativePos_y = obj.Y + 180;
		sessionStorage.current_menu_silder_max = maxBulltin;
		sessionStorage.current_menu_silder_radios = bulltion_small_radios;
		var bk_grap = new createjs.Graphics();
		bk_grap.setStrokeStyle(1).beginStroke(createjs.Graphics.getRGB(0,0,0)).beginFill(createjs.Graphics.getRGB(255,255,255)).drawRoundRectComplex(obj.X,obj.Y+165,300,35,0,0,15,15);
		var slider_box = new createjs.Shape(bk_grap);
		slider_box.alpha = 0.7;
		for (var i = 1; i <= maxBulltin; i++) {
			bk_grap = new createjs.Graphics();
			bk_grap.setStrokeStyle(1).beginStroke(createjs.Graphics.getRGB(0,0,0)).beginFill(createjs.Graphics.getRGB(255,255,255)).drawCircle(relativePos_x , relativePos_y,bulltion_radios);
			var bulltion_big = new createjs.Shape(bk_grap);
			bulltion_big.idx = i;
			bulltion_big.name= "slider_btn_"+i;
			bulltion_big.is_slider_btn = true;
			bk_grap = new createjs.Graphics();
			bk_grap.setStrokeStyle(1).beginStroke(createjs.Graphics.getRGB(255,255,255)).beginFill(createjs.Graphics.getRGB(255,255,255)).drawCircle(relativePos_x   , relativePos_y ,bulltion_small_radios);
			var bulltion_small = new createjs.Shape(bk_grap);
			bulltion_small.last_x = relativePos_x;
			bulltion_small.last_y = relativePos_y;
			bulltion_small.last_idx = i;
			bulltion_small.idx = "s"+bulltion_big.id;
			bulltion_big.onMouseOver =function(evt) {
				var target = evt.target;
				//reset all markers
				obj._reset_slider_btns();
				sessionStorage.is_blocked = 1;
				var idx = obj.slider_bullton_Continer.getChildIndex (target);
				var childElem = _.find(obj.slider_bullton_Continer.children , function (item) {return item.idx == "s"+target.id});
				obj.changeSliderImage(childElem.last_idx);
				var pos_x = childElem.last_x;
				var pos_y = childElem.last_y;
				sessionStorage.current_menu_silder_idx = childElem.last_idx;
				childElem.graphics.clear().setStrokeStyle(1).beginStroke(createjs.Graphics.getRGB(255,255,255)).beginFill(createjs.Graphics.getRGB(0,0,0)).drawCircle(pos_x  , pos_y ,bulltion_small_radios);
			}
			bulltion_big.onMouseOut = function(evt) {
				var target = evt.target;
				sessionStorage.is_blocked = 0;
				var idx = obj.slider_bullton_Continer.getChildIndex (target);
				var childElem = _.find(obj.slider_bullton_Continer.children , function (item) {return item.idx == "s"+target.id});
				var pos_x = childElem.last_x;
				var pos_y = childElem.last_y;
				obj.changeSliderImage(sessionStorage.current_menu_silder_idx);
				obj.timer_current_time = moment();
				childElem.graphics.clear().setStrokeStyle(1).beginStroke(createjs.Graphics.getRGB(255,255,255)).beginFill(createjs.Graphics.getRGB(255,255,255)).drawCircle(pos_x  , pos_y ,bulltion_small_radios);
				obj.backLastKnownPosSlider();
			}
			obj.slider_bullton_Continer.addChild(bulltion_big);
			obj.slider_bullton_Continer.addChild(bulltion_small);
			relativePos_x+=25;
		};
		obj.continer.addChild(obj.slider_bullton_Continer);
		obj.continer.addChild(slider_box);
	}
	NarutoGame_Logic.Menu.Animation = object;
})