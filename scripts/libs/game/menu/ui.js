define([
	// Standard Libs
	'Console'		// lib/console/console
	, 'jQuery'	// lib/underscore/underscore
	, 'Underscore'	// lib/underscore/underscore
	, 'Moment'	// lib/moment/moment
	, 'controllers/TestController'
	// Basic Vendors Libs For the Game
	, "PreloadJs"
	, "EaseJs"
	, "TweenJs"
	, "SoundJs"
], function (Console,$, _,moment,testLevel) {
	"use strict";
	var object = function(props) { this.initialize(props); };
	var obj = object.prototype = new createjs.Container();
	obj.Container_initialize = obj.initialize;
	obj.events = {};
	obj.events.onInitAssetPipe = function (){
		obj.ApplicationObject.stage.autoClear = false;
	}
	obj.events.onWaitingAssetPipe = function (){}
	obj.events.onMenuButtonClick = function (evt) {
		if (evt.target.action == "playnow"){
			window.location ="#/test";
			var _scope = angular.element($('body')).scope();
			//angular.element(domElement).scope().changeMap(NarutoGame_Maps.Test,10,10);
			//testLevel.$emit('changeMap', NarutoGame_Maps.Test,10,10);
		}
		console.log(evt);
	}
	obj.events.onCompleteAssetPipe = function (assets){
		obj.assets = assets;
		console.log("[Menu Assets Loaded]",assets);
		obj.initDisplay(assets);
	}
	obj.initialize = function (props) {
		var _this = this;
		if (props) {
			obj.Container_initialize();
			obj.ApplicationObject = props.app;
			obj.props = props;
			obj.assetPipe = new NarutoGame_Logic.AssetPipe(menu_menifiest,obj.events.onInitAssetPipe , obj.events.onWaitingAssetPipe,obj.events.onCompleteAssetPipe);
			obj.assetPipe.Load();
		}
	}
	obj.initDisplay = function () {

		Console.group("Game Menu.");
		obj.buildMainScreen();
		obj.buildMainScreen_buttons();
		obj.buildMainScreen_animation();

		Console.groupEnd();
	}
	obj.buildMainScreen_animation = function () {
		var bk_grap = new createjs.Graphics();
		var continer =new createjs.Container(); 
		var animation_pos_x = 200;
		var animation_pos_y = 50;

		bk_grap.setStrokeStyle(1).beginStroke(createjs.Graphics.getRGB(0,0,0)).beginFill(createjs.Graphics.getRGB(0,0,0)).drawRoundRect(animation_pos_x,animation_pos_y,300,200,15);
		var animation_box = new createjs.Shape(bk_grap);
		animation_box.alpha = 0.6;
		var animation_content = new NarutoGame_Logic.Menu.Animation({
			app: obj.ApplicationObject,
			x: animation_pos_x,
			y: animation_pos_y
		});
		Console.info(animation_content);
		continer.addChild(animation_box);
		continer.addChild(animation_content);
		obj.addChild(continer);
	}

	obj.buildMainScreen_buttons = function () {
		var bk_grap = new createjs.Graphics();
		var continer =new createjs.Container(); 
		continer.mouseEnabled =  true;
		var buttons = _.find(obj.assets,function (item) {return item.id == "menu_buttons"});
		Console.info("load game asset:", buttons);
		var dataIconSet = {
			images:[buttons.result],
			frames:{width:248, height:16},
			animations: {active:0, notactive:1, hover:2, disable:3}
		}
		var spriteSheet  = new createjs.SpriteSheet(dataIconSet);
		var asset_menu_button_icon = new createjs.BitmapAnimation(spriteSheet);
		var aboutUs_Button =  new NarutoGame_Logic.Menu.Button({
			app: obj.ApplicationObject,
			id: "aboutus",
			asset: asset_menu_button_icon.clone(),
			X: 100,
			Y: 0,
			stateNormal: 'active',
			stateHover: 'hover',
			text: 'About Us' ,
			font: "11px Exo",
			color: "#FFF",
			hover: "#000",
		});
		Console.info("Wire Event",aboutUs_Button);
		aboutUs_Button.action = "about";
		aboutUs_Button.onMouseOver = function(evt) {
			var target = evt.target;
			var asset = target.asset;
			asset.gotoAndStop('hover');
		}
		aboutUs_Button.onMouseOut = function(evt) {
			var target = evt.target;
			var asset = target.asset;
			asset.gotoAndStop('active');
		}
		aboutUs_Button.onClick = obj.events.onMenuButtonClick;
		var help_Button =  new NarutoGame_Logic.Menu.Button({
			app: obj.ApplicationObject,
			id: "help",
			asset: asset_menu_button_icon.clone(),
			X: 100,
			Y: 0,
			stateNormal: 'active',
			stateHover: 'hover',
			text: 'Help' ,
			font: "11px Exo",
			color: "#FFF",
			hover: "#000",
			callback: obj.events.onMenuButtonClick
		});
		help_Button.action = "help";
		help_Button.y = 16;
		Console.info("Wire Event",help_Button);
		help_Button.onMouseOver =function(evt) {
			var target = evt.target;
			var asset = target.asset;
			asset.gotoAndStop('hover');
		}
		help_Button.onMouseOut = function(evt) {
			var target = evt.target;
			var asset = target.asset;
			asset.gotoAndStop('active');
		}
		help_Button.onClick = obj.events.onMenuButtonClick;
		var playNow_Button =  new NarutoGame_Logic.Menu.Button({
			app: obj.ApplicationObject,
			id: "playnow",
			asset: asset_menu_button_icon.clone(),
			X: 100,
			Y: 0,
			stateNormal: 'active',
			stateHover: 'hover',
			text: 'Play Now!' ,
			font: "11px Exo",
			color: "#FFF",
			hover: "#000",
			callback: obj.events.onMenuButtonClick
		});
		playNow_Button.y = 56;
		Console.info("Wire Event",playNow_Button);
		playNow_Button.action = "playnow";
		playNow_Button.onMouseOver = function(evt) {
			var target = evt.target;
			var asset = target.asset;
			asset.gotoAndStop('hover');
		}
		playNow_Button.onMouseOut = function(evt) {
			var target = evt.target;
			var asset = target.asset;
			asset.gotoAndStop('active');
		}
		playNow_Button.onClick = obj.events.onMenuButtonClick;
		continer.x = 37;
		continer.y = 450;
		continer.addChild(playNow_Button);
		continer.addChild(aboutUs_Button);
		continer.addChild(help_Button);
		obj.addChild(continer);
	}
	obj.buildMainScreen = function () {
		var background_asset = _.find(obj.assets,function (item) {return item.id == "menu_background"});
		var background_side_asset = _.find(obj.assets,function (item) {return item.id == "menu_background_side"});
		var background_screen_asset = _.find(obj.assets,function (item) {return item.id == "menu_background_screen"});
		var background_screen_letters_w_asset = _.find(obj.assets,function (item) {return item.id == "menu_screen_letters_w"});
		Console.info("load game asset:", background_asset);
		var bk_grap = new createjs.Graphics();
		bk_grap.beginBitmapFill(background_asset.result);
		var background = new createjs.Shape(bk_grap.drawRect(0,0,obj.ApplicationObject.canvasWidth,obj.ApplicationObject.canvasHeight));
		Console.info("load game asset:", background_side_asset);
		bk_grap = new createjs.Graphics();
		bk_grap.beginBitmapFill(background_side_asset.result,"no-repeat");

		var background_side = new createjs.Shape(bk_grap.drawRect(0,0,background_side_asset.width,background_side_asset.height));
		background_side.alpha = 0.5
		background_side.x = 700;
		background_side.y = 400;
		background_side.rotation = 30; 

		bk_grap = new createjs.Graphics();
		bk_grap.setStrokeStyle(1).beginStroke(createjs.Graphics.getRGB(0,0,0)).beginFill(createjs.Graphics.getRGB(0,0,0)).drawRoundRect(0,300,background_screen_asset.width+10,background_screen_asset.height+10,15);
		var background_screen_border = new createjs.Shape(bk_grap);
		background_screen_border.alpha = 0.6;
		Console.info("load game asset:", background_screen_asset);
		bk_grap = new createjs.Graphics();
		bk_grap.beginBitmapFill(background_screen_asset.result,"no-repeat");
		var background_screen = new createjs.Shape(bk_grap.drawRect(0,0,background_screen_asset.width,background_screen_asset.height));
		background_screen.alpha = 0.5;
		background_screen.x = 0;
		background_screen.y = 300;

		Console.info("load game asset:", background_screen_letters_w_asset);
		bk_grap = new createjs.Graphics();
		bk_grap.beginBitmapFill(background_screen_letters_w_asset.result,"no-repeat");
		bk_grap.filters = [new createjs.BoxBlurFilter(120,  100, 2), new createjs.ColorMatrixFilter(new createjs.ColorMatrix(0,0,-100,0))];
		//bk_grap.updateCache();
		
		var background_screen_letters_w = new createjs.Shape(bk_grap.drawRect(0,0,background_screen_letters_w_asset.width,background_screen_letters_w_asset.height));

		background_screen_letters_w.alpha = 0.8;
		background_screen_letters_w.x = 620;
		background_screen_letters_w.y = 550;
		var gameLogo =new NarutoGame_Logic.Commons.TextLink("Naruto 2D game", "25px Rancho" , "#FFF","#C3C3C3");
		gameLogo.x = 20;
		gameLogo.y = 350;
		//add elements 
		obj.addChild(background);
		obj.addChild(background_side);
		obj.addChild(background_screen_border);
		obj.addChild(background_screen);
		obj.addChild(background_screen_letters_w);
		obj.addChild(gameLogo);

		Console.info("Main Menu Screen Build!");
	}
	obj.toString = function() {
		return "[Main Menu]";
	};
	NarutoGame_Logic.Menu.UI = object;
})