define([
	// Standard Libs
	'Console'		// lib/console/console
	, 'jQuery'	// lib/underscore/underscore
	, 'Underscore'	// lib/underscore/underscore
	, 'Moment'	// lib/moment/moment
	// Basic Vendors Libs For the Game
	, "PreloadJs"
	, "EaseJs"
	, "TweenJs"
	, "SoundJs"
], function (Console,$, _,moment) {
	"use strict";
	var object = function(props) { this.initialize(props); };
	var obj = object.prototype = new createjs.Container();
	obj.Container_initialize = obj.initialize;
	obj.events = {};

	obj.initialize = function (props) {
		var _this = this;
		if (props) {
			_this.Container_initialize();
			obj.ApplicationObject = props.app;
			_this.props = props;
			_this.id = props.id;
			_this.asset = props.asset;
			_this.posX = props.X || 0;
			_this.posY = props.Y || 0;
			_this.text = props.text;
			_this.font = props.font || "11px Arial"
			_this.color = props.color || "#FFF";
			_this.hover = props.hover || props.color;
			_this.state_normal = props.stateNormal;
			_this.buildButton();
		}
	}

	obj.buildButton = function () {
		var _this = this;
		var continer =new createjs.Container(); 
		var link = new NarutoGame_Logic.Commons.TextLink(_this.text ,_this.font,_this.color,_this.hover);
		link.x = _this.posX;
		link.y = _this.posY;
		_this.asset.gotoAndStop(_this.state_normal);
		continer.addChild(_this.asset);
		continer.addChild(link);
		_this.addChild(continer);
	}
	NarutoGame_Logic.Menu.Button = object;
})