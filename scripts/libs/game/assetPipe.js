define([
	// Standard Libs
	'Console'		// lib/console/console
	, 'jQuery'	// lib/underscore/underscore
	, 'Underscore'	// lib/underscore/underscore
	// Basic Vendors Libs For the Game
	, "PreloadJs"
	, "EaseJs"
	, "TweenJs"
	, "SoundJs"
], function (Console,$, _) {
	"use strict";
	var object = function (manifest,initCallback,waitingCallback,completeCallback) {
		var _this = this;
		//local params
		var loader = new createjs.PreloadJS();
		loader.useXHR = false;  // XHR loading is not reliable when running locally.
		var _tmpAssets = [];
		var assets = [];

		//global params
		_this.manifiest = manifest;
		_this.callbacks = {
			init: initCallback,
			waiting: waitingCallback,
			complete: completeCallback
		}
		/**
		 * will load assets from the manifiest object
		 * @param {object} manifest         manifest assest data
		 * @param {function} initCallback     on init 
		 * @param {function} waitingCallback  on wait
		 * @param {function} completeCallback on complate
		 */
		_this.Load = function () {
			loader.onFileLoad = handleFileLoad;
			loader.onComplete = handleComplete;
			if (_.isFunction(_this.callbacks.init))
				_this.callbacks.init.call();
			loader.loadManifest(_this.manifiest);
			function handleFileLoad(event) {
				_tmpAssets.push(event);
				if (_.isFunction(_this.callbacks.waiting))
					_this.callbacks.waiting.call();
			}
			function handleComplete() {
				_.each(_tmpAssets , function (item) {
					//var item = _tmpAssets[i]; //loader.getResult(id);

          var _w = item.result.width;
          var _h = item.result.height;
					var id = item.id;
					var rawData =_this.getBase64Image(item.result);
					var result = item.result;
					var bmp = null;
					if (item.type == createjs.PreloadJS.IMAGE) {
						var bmp = new createjs.Bitmap(result);
					}
					var assetObject = {
						id: id,
						result: result,
						width: _w, 
						height: _h,
						rawData: rawData,
						bmp: bmp
					}
					assets.push(assetObject);
				})
				if (_.isFunction(_this.callbacks.complete))
					_this.callbacks.complete.call(this,assets);
			}
		}
		_this.getBase64Image = function (img) {  
		  var canvas = document.createElement("canvas");  
		  canvas.width = img.width;  
		  canvas.height = img.height;  
		  var ctx = canvas.getContext("2d");  
		  ctx.drawImage(img, 0, 0);  
		  var dataURL = canvas.toDataURL("image/png");  
		  // escape data:image prefix
		  //return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");  
		  // or just return dataURL
		  return dataURL
		}  
	}
	NarutoGame_Logic.AssetPipe = object;
})