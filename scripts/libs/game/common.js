define([
	// Standard Libs
	'Console'		// lib/console/console
	, 'jQuery'	// lib/underscore/underscore
	, 'Underscore'	// lib/underscore/underscore
	// Basic Vendors Libs For the Game
	, "PreloadJs"
	, "EaseJs"
	, "TweenJs"
	, "SoundJs"
], function (Console,$, _) {
	"use strict";
	window.NarutoGame_Logic = {
		Commons: {},
		Maps: {},
		Menu: {}
	};
	window.NarutoGame_Maps = {
		Test:0
	}
	window.NarutoGame = {};
});