define([
	// Standard Libs
	'Console'		// lib/console/console
	, 'jQuery'	// lib/underscore/underscore
	, 'Underscore'	// lib/underscore/underscore
	// Basic Vendors Libs For the Game
	, "PreloadJs"
	, "EaseJs"
	, "TweenJs"
	, "SoundJs"
], function (Console,$, _) {
	"use strict";
	var object = function(props) { this.initialize(props); };
	var obj = object.prototype = new createjs.Container();
	obj.Container_initialize = obj.initialize;
	obj.events = {};
	obj.events.onInitAssetPipe = function (){
		obj.ApplicationObject.stage.autoClear = false;
	}
	obj.events.onWaitingAssetPipe = function (){}
	obj.events.onCompleteAssetPipe = function (assets){
		obj.assets = assets;
		console.log("[Maps Assets Loaded]",assets);
		console.log("[Level Pre init start]",assets);
		obj.initMap(assets);
	}

	obj.initialize = function (props) {
		var _this = this;
		if (props) {
			_this.Container_initialize();
			obj.ApplicationObject = props.app;
			obj.props = props;
			obj.map_type = props.mapType || NarutoGame_Maps.Test;
			obj.rows = props.row || 10;
			obj.cols = props.cols || 10;
			Console.group("Maps");
			Console.info("load Assets");
			obj.assetPipe = new NarutoGame_Logic.AssetPipe(maps_menifiest,obj.events.onInitAssetPipe , obj.events.onWaitingAssetPipe,obj.events.onCompleteAssetPipe);
			obj.assetPipe.Load();
		}
	}

	obj.initMap = function (assets) {
		var _this = this;
		var continer = new createjs.Container();
		var map_object = null;
		Console.info("Finshed Load Assets",assets);
		switch (obj.map_type) {
			case NarutoGame_Maps.Test:
				Console.info("init map");

				var asset_Data = _.find(assets,function (item) { return item.id == "map_elems_1"});
				map_object = new NarutoGame_Logic.Maps.Level_Test({
					app: obj.ApplicationObject,
					cols:obj.cols,
					rows:obj.rows,
					asset:asset_Data
				});
				continer.addChild(map_object);

				Console.info("added Map",map_object.toString());
				break;
			default:
				console.log("[Map Not Found Can't load map]");
				throw "[Map Not Found Can't load map]";
				break;
		}
		Console.info("End Map Loading");
		Console.groupEnd();
		obj.addChild(continer);
	}
	NarutoGame_Logic.Maps.Manager = object;
})