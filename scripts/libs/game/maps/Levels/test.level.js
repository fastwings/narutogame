define([
	// Standard Libs
	'Console'		// lib/console/console
	, 'jQuery'	// lib/underscore/underscore
	, 'Underscore'	// lib/underscore/underscore
	// Basic Vendors Libs For the Game
	, "PreloadJs"
	, "EaseJs"
	, "TweenJs"
	, "SoundJs"
], function (Console,$, _) {
	"use strict";
	var object = function(props) { this.initialize(props); };
	var obj = object.prototype = new createjs.Container();
	obj.Container_initialize = obj.initialize;
	obj.events = {};
	obj.imageClassName = ".level_background";
	obj.initialize = function (props) {
		var _this = this;
		if (props) {
			_this.Container_initialize();
			obj.ApplicationObject = props.app;
			obj.cols = props.cols;
			obj.rows = props.rows;
			obj.asset = props.asset;

			Console.group("Map Test");
			Console.info("Init" ,props);
			obj.initLevel();
		}
	}

	obj.initLevel = function () {
		var _this = this;
		var continer = new createjs.Container();
		var t_width = 30;
		var t_height = 30;
		obj.width = obj.cols * t_width;
		obj.titlset_width = t_width;
		obj.height = obj.rows * t_height;
		obj.titlset_height = t_height;
		obj._drawMap();
	}
	obj._drawMap = function () {
		var _this = this;

		Console.info("pre Draw");
		Console.info("Start Draw" , moment());
		var container = new createjs.Container();
		$(obj.imageClassName).css("background-image", _.str.sprintf("url(%s)",obj.asset.rawData));

		var bk_grap = new createjs.Graphics();
		var elem = $("div.ground_send");
		var dom = elem.get(0);
		var current_x = 0;
		var current_y = 0;
		for (var i = 0; i<=obj.cols;i++,current_x+=obj.titlset_height) {
			for (var i = 0; i<=obj.rows;i++,current_y+=obj.titlset_height) {
				var elem =  new createjs.DOMElement(dom);
				bk_grap.beginStroke(createjs.Graphics.getRGB(0,0,0)).beginFill(createjs.Graphics.getRGB(0,0,0)).drawRect(current_x,current_y,obj.titlset_width,obj.titlset_height);
				var shape_box = new createjs.Shape(bk_grap);
				container.addChild(shape_box);
				container.addChild(elem);
			};
			var elem =  new createjs.DOMElement(dom);
			bk_grap.beginStroke(createjs.Graphics.getRGB(0,0,0)).beginFill(createjs.Graphics.getRGB(0,0,0)).drawRect(current_x,current_y,obj.titlset_width,obj.titlset_height);
			var shape_box = new createjs.Shape(bk_grap);
			container.addChild(shape_box);
			container.addChild(elem);
		};

		Console.info("End Draw" , moment());
		Console.info("Post Draw");
		Console.groupEnd();
		obj.addChild(container);
	}

	object.prototype.toString = function() {
		return "[Map object (Test)]";
	};
	NarutoGame_Logic.Maps.Level_Test = object;
})