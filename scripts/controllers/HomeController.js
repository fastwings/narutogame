define(['Console'], function (Console) {
	"use strict";
	Console.group("Entering HomeController module.");
	
	var controller = function () {
		Console.group("HomeController entered.");

		var scope = this;

		scope.addItemProcess2 = function () {
		    scope.$apply(function () {
		        scope.navigationItems[0].ItemsCount = $scope.navigationItems[0].ItemsCount + 1;
		    });
		};
		Console.groupEnd();
	};
	controller.$inject = [];

	Console.groupEnd();
	return controller;
});