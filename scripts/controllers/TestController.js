define(['Console'], function (Console) {
	"use strict";
	Console.group("Entering TestController module.");
	
	var controller = function () {
		Console.group("TestController entered.");

		var scope = this;
		//as we entered need change the map 
		NarutoGameINS.ChangeMap(NarutoGame_Maps.Test,10,10);
		Console.groupEnd();
	};
	controller.$inject = [];

	Console.groupEnd();
	return controller;
});