"use strict";

require.config({
	paths: {
		Console: 'libs/vendors/console/console'
		, jQuery: 'libs/vendors/jquery/loader'//'http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min'
		, Underscore: 'libs/vendors/underscore/loader'
		, Moment: 'libs/vendors/moment/loader'
		, EaseJs: 'libs/vendors/easejs/loader'
		, PreloadJs: 'libs/vendors/preloadjs/loader'
		, TweenJs: 'libs/vendors/tweenjs/loader'
		, SoundJs: 'libs/vendors/soundjs/loader'
		, NarutoGame: 'libs/game/loader'
		, Angular: 'libs/angular/angular'
	}
	, priority: [ 
		"Console"
		, "jQuery"
		, "Underscore"
		, "PreloadJs"
		, "EaseJs"
		, "TweenJs"
		, "SoundJs"
		, "NarutoGame"
		, "Angular" 
	]
	, urlArgs: 'v=1.0&_'//+new Date()
});

require([
	// Standard Libs
	'require'
	, 'Console'
	, 'jQuery'
	, 'Underscore'
	, 'Angular'
], function (require, Console, $, _, angular) {
	Console.group("Bootstrap dependencies loaded. Starting bootstrap.");
	Console.info("Console", Console);
	Console.info("jQuery", $);
	Console.info("Underscore: ", _);
	Console.info("Angular: ", angular);

	require(['app'], function (App) {
		Console.group("Starting bootstrap.");
		Console.info("App: ", App);

		App.initialize();
		
		Console.groupEnd();
	});

	Console.groupEnd();
});